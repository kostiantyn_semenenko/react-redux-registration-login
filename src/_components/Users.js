import React, { Component } from 'react'

import { userActions } from '../_actions';

export default class Users extends Component {

  componentDidMount() {
      this.props.getUsers();
  }

  handleDeleteUser(id) {
      return (e) => this.props.deleteUser(id);
  }

  render() {
    return (
      <div>
      <h3>All registered users:</h3>
        {users.loading && <em>Loading users...</em>}
        {users.error && <span className="text-danger">ERROR: {users.error}</span>}
        {users.items &&
            <ul>
                {users.items.map((user, index) =>
                    <li key={user.id}>
                        {user.firstName + ' ' + user.lastName}
                        {
                            user.deleting ? <em> - Deleting...</em>
                            : user.deleteError ? <span className="text-danger"> - ERROR: {user.deleteError}</span>
                            : <span> - <a onClick={this.handleDeleteUser(user.id)}>Delete</a></span>
                        }
                    </li>
                )}
            </ul>
        }
      </div>
    )
  }
}
